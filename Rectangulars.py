def if_exist(weight, height):
    if weight > 0 and height > 0:
        return True
    else:
        return False


class Rectangular(object):
    def __init__(self, weight, height):
        self.w = weight
        self.h = height
        self.if_exist = if_exist(self.w, self.h)

    def calculate_square(self):
        return self.w ** 2

    def calculate_polygon(self):
        return (self.w + self.h) * 2

    def check_type(self):
        if self.if_exist:
            if self.w == self.h:
                return 'Square'
            else:
                return 'Polygon'


figure1 = Rectangular(int(input()), int(input()))
if figure1.check_type() == 'Square':
    print(figure1.calculate_square(), figure1.check_type())
elif figure1.check_type() == 'Polygon':
    print(figure1.calculate_polygon(), figure1.check_type())
