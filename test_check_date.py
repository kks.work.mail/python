import unittest
from checkMyDate import check_my_date


class CalcTest(unittest.TestCase):
    def test_true_date1(self):
        self.assertEqual(check_my_date("12/12/2000"), 'True')

    def test_true_date2(self):
        self.assertEqual(check_my_date("29/02/2000"), 'True')

    def test_false_date1(self):
        self.assertEqual(check_my_date("32/11/2000"), 'False')

    def test_false_date2(self):
        self.assertEqual(check_my_date("30/02/2000"), 'False')

    def test_false_date3(self):
        self.assertEqual(check_my_date("12/13/2000"), 'False')

    def test_false_date4(self):
        self.assertEqual(check_my_date("12/12/123"), 'False')


if __name__ == '__main__':
    unittest.main()
