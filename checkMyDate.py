import time


def check_my_date(date):
    try:
        time.strptime(date, '%d/%m/%Y')
        return 'True'
    except ValueError:
        return 'False'


print(check_my_date(input('Please enter the date in the format DD/MM/YYYY \n')))

