import unittest
from Rectangulars import Rectangular
from Rectangulars import if_exist


class TestRectangular(unittest.TestCase):
    def test_exist_true1(self):
        self.assertEqual(if_exist(2, 2), 'True')

    def test_exist_false1(self):
        self.assertEqual(if_exist(0, 1), 'False')

    def test_exist_false2(self):
        self.assertEqual(if_exist(0, 0), 'False')

    def test_exist_false3(self):
        self.assertEqual(if_exist(1, 0), 'False')

    def test_type1(self):
        self.assertEqual(Rectangular(2, 2).check_type(), 'Square')

    def test_type2(self):
        self.assertEqual(Rectangular(3, 2).check_type(), 'Polygon')

    def test_calc_square(self):
        self.assertEqual(Rectangular(2, 2).calculate_square(), 4)

    def test_calc_polygon(self):
        self.assertEqual(Rectangular(3, 2).calculate_polygon(), 10)


if __name__ == '__main__':
    unittest.main()
